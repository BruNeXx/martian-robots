require=(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
//** instantiates a new 'mars' grid
var Mars = function(x, y){
	this.x = x;
	this.y = y;
	this.dangerPositions = [];
	this.robots = [];
	return this;
};

//** checks if a x,y is valid on this "mars" and returns true/false
Mars.prototype.validPostition = function(x,y){
	if(x > this.x || y > this.y) return false;
	else return true;
};

//** traverses all logged danger postitions and returns true/false
Mars.prototype.dangerPosition = function(position){
	for (i = 0, iMax = this.dangerPositions.length; i < iMax; i += 1) {
		var dangerPosition = this.dangerPositions[i];
		if(position.x === dangerPosition.x && position.y === dangerPosition.y && position.direction === dangerPosition.direction) return true;
		else return false;
	}
};
module.exports = Mars;

},{}],2:[function(require,module,exports){
//** helper dictionary to translate angles to directions
var getDirectionAngle = {
  'N': 90,
  'S': 270,
  'W': 180,
  'E': 0
};

//** helper dictionary to translate directions to angles
var getAngleDirection = {
   '90' : 'N',
   '270' :'S',
   '180' :'W',
   '0' : 'E'
};

//** instantiates a new robot with a 'x,y' position and a direction
var Robot = function (x, y, direction) {
  this.x = x;
  this.y = y;
  this.lost = false;
  this.direction = direction;
  this.angle = getDirectionAngle[direction];
  return this;
};

//** turn the robot 90 degrees, recives 'L' for left and 'R' for right
Robot.prototype.turn = function(where){
  if(where == 'L') {
    this.angle += 90;
    if(this.angle >= 360) this.angle = this.angle - 360;
    this.direction = (getAngleDirection[this.angle]);
  }else if(where == 'R') {
    this.angle -= 90;
    if(this.angle < 0) this.angle = this.angle + 360;
    this.direction = (getAngleDirection[this.angle]);
  }
};

//** moves the robot depending on the direction
Robot.prototype.move = function(){
  if(this.direction == 'N') this.y += 1;
  if(this.direction == 'S') this.y -= 1;
  if(this.direction == 'W') this.x -= 1;
  if(this.direction == 'E') this.x += 1;
};

module.exports = Robot;

},{}],"FyzPWh":[function(require,module,exports){
var Mars = require('./Mars.js');
var Robot = require('./Robot.js');

module.exports.parseData = function parseData(data){
	//TODO: review/refactor this regex problably have some bugs (but works with the example instructions)...
	var mars_dimensions = data.match(/(\d\s\d.*\n){1}/g);
	var robots_instructions = data.match(/(\d\s\d\s[NSEW]{1}.*\n.*[LRF]){1,}/g);

	return {
		mars_dimensions:{
			x: mars_dimensions[0].split(' ')[0],
			y: mars_dimensions[0].split(' ')[1]
		},
		robots_instructions: robots_instructions
	};
}

module.exports.createMars = function createMars(x, y){
	var marsX = parseInt(x.trim());
	var marsY = parseInt(y.trim());
	//TODO: validate x and y to make sure they were corrected parsed...
	return new Mars(marsX,marsY);
}



//** recives mars grid, and a array of robot instructions
module.exports.sendRobots = function sendRobots(mars, robots_instructions){
	robots_instructions.forEach(function(robot_intructions){
		//** Parse the robot starting position
		robot_intructions = robot_intructions.split('\n');
		var x = parseInt(robot_intructions[0][0]);
		var y = parseInt(robot_intructions[0][2]);

		//** create the robot with the starting position and "send it to mars"
		var robot = new Robot(x, y, robot_intructions[0][4]);
		mars.robots.push(robot);

		//** Parse instructions and move the robot
		controlRobot(mars, robot, robot_intructions[1]);
	});
}

//** recieves mars grid, a robot and instructions string
function controlRobot(mars, robot, instructions){
	//** loop the instructions string
	for (var i = 0, iMax = instructions.length; i < iMax; i += 1) {
		var instruction = instructions[i];
		if(instruction ==='F'){ //** check if is a instruction to move the robot
			var lastPosition = {x: robot.x, y:robot.y, direction: robot.direction}; //** rebember the last position in case it got lost
			//** Check if a danger position
			if (!mars.dangerPosition(lastPosition)){
				robot.move();
			}
			//** Check if the new position is valid
			if(!mars.validPostition(robot.x, robot.y)){
				robot.lost = true; // mark the robot as lost
				mars.dangerPositions.push(lastPosition); // add the last position to danger positions
			}
		}else if(instruction === 'R' || instruction === 'L'){ //** check if is a instruction to turn the robot
			robot.turn(instruction);
		}
	}

}

},{"./Mars.js":1,"./Robot.js":2}],"app":[function(require,module,exports){
module.exports=require('FyzPWh');
},{}]},{},[])