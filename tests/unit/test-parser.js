var test = require('tap').test;
var martianRobots = require('../../src');


test('Test parser', function (t) {
    t.plan(2);
    var sampleInput = "5 3 \n1 1 E \nRFRFRFRF \n3 2 N \nFRRFLLFFRRFLL \n0 3 W \nLLFFFLFLFL ";
    var data = martianRobots.parseData(sampleInput);
    t.equal(data.mars_dimensions.x, '5');
    t.equal(data.mars_dimensions.y, '3');
})
