var test = require('tap').test;
var Robot = require('../../src/Robot.js');
var robot = new Robot(1,1, 'E');

test('Test robot', function(t){
  t.plan(3);
  robot.turn('R')
  robot.move('F')
  robot.turn('R')
  robot.move('F')
  robot.turn('R')
  robot.move('F')
  robot.turn('R')
  robot.move('F')

  t.equal(robot.x, 1);
  t.equal(robot.y, 1);
  t.equal(robot.direction, 'E');
})
