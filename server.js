var http = require('http');
var filed = require("filed");

http.createServer(function(req, res) {
    if (req.url == '/' || req.url == '/index.html') filed(__dirname + "/index.html").pipe(res);
    else if (req.url == '/bin/app.js') filed(__dirname + "/bin/app.js").pipe(res);
    else res.end('notfound...');
}).listen(3000);
