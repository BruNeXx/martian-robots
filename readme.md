I used browserify to bundle all the javascript files you can read more at http://browserify.org/

### How to run:

`$ sudo npm install browserify -g`

`$ npm install`

`$ npm start`

It will spwan a server on http://localhost:3000/

You can find the source javascript at ./src and the html at ./index.html

If you don't have node you can look at ./bin and see the compiled
javascript... and if you open the ./index.html from your file system it will also work
**(I think)** if you prefer i can also send everything in a single .js file without browserify...



### How to run the tests:

`$ sudo npm install tap -g`

`$ npm install`

`$ npm test`

I've done only two unit tests, to test the parser and the robot movements, I can implement more tests if needed...
