//** instantiates a new 'mars' grid
var Mars = function(x, y){
	this.x = x;
	this.y = y;
	this.dangerPositions = [];
	this.robots = [];
	return this;
};

//** checks if a x,y is valid on this "mars" and returns true/false
Mars.prototype.validPostition = function(x,y){
	if(x > this.x || y > this.y) return false;
	else return true;
};

//** traverses all logged danger postitions and returns true/false
Mars.prototype.dangerPosition = function(position){
	for (i = 0, iMax = this.dangerPositions.length; i < iMax; i += 1) {
		var dangerPosition = this.dangerPositions[i];
		if(position.x === dangerPosition.x && position.y === dangerPosition.y && position.direction === dangerPosition.direction) return true;
		else return false;
	}
};
module.exports = Mars;
