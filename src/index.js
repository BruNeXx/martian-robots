var Mars = require('./Mars.js');
var Robot = require('./Robot.js');

module.exports.parseData = function parseData(data){
	//TODO: review/refactor this regex problably have some bugs (but works with the example instructions)...
	var mars_dimensions = data.match(/(\d\s\d.*\n){1}/g);
	var robots_instructions = data.match(/(\d\s\d\s[NSEW]{1}.*\n.*[LRF]){1,}/g);

	return {
		mars_dimensions:{
			x: mars_dimensions[0].split(' ')[0],
			y: mars_dimensions[0].split(' ')[1]
		},
		robots_instructions: robots_instructions
	};
}

module.exports.createMars = function createMars(x, y){
	var marsX = parseInt(x.trim());
	var marsY = parseInt(y.trim());
	//TODO: validate x and y to make sure they were corrected parsed...
	return new Mars(marsX,marsY);
}



//** recives mars grid, and a array of robot instructions
module.exports.sendRobots = function sendRobots(mars, robots_instructions){
	robots_instructions.forEach(function(robot_intructions){
		//** Parse the robot starting position
		robot_intructions = robot_intructions.split('\n');
		var x = parseInt(robot_intructions[0][0]);
		var y = parseInt(robot_intructions[0][2]);

		//** create the robot with the starting position and "send it to mars"
		var robot = new Robot(x, y, robot_intructions[0][4]);
		mars.robots.push(robot);

		//** Parse instructions and move the robot
		controlRobot(mars, robot, robot_intructions[1]);
	});
}

//** recieves mars grid, a robot and instructions string
function controlRobot(mars, robot, instructions){
	//** loop the instructions string
	for (var i = 0, iMax = instructions.length; i < iMax; i += 1) {
		var instruction = instructions[i];
		if(instruction ==='F'){ //** check if is a instruction to move the robot
			var lastPosition = {x: robot.x, y:robot.y, direction: robot.direction}; //** rebember the last position in case it got lost
			//** Check if a danger position
			if (!mars.dangerPosition(lastPosition)){
				robot.move();
			}
			//** Check if the new position is valid
			if(!mars.validPostition(robot.x, robot.y)){
				robot.lost = true; // mark the robot as lost
				mars.dangerPositions.push(lastPosition); // add the last position to danger positions
			}
		}else if(instruction === 'R' || instruction === 'L'){ //** check if is a instruction to turn the robot
			robot.turn(instruction);
		}
	}

}
