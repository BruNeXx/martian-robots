//** helper dictionary to translate angles to directions
var getDirectionAngle = {
  'N': 90,
  'S': 270,
  'W': 180,
  'E': 0
};

//** helper dictionary to translate directions to angles
var getAngleDirection = {
   '90' : 'N',
   '270' :'S',
   '180' :'W',
   '0' : 'E'
};

//** instantiates a new robot with a 'x,y' position and a direction
var Robot = function (x, y, direction) {
  this.x = x;
  this.y = y;
  this.lost = false;
  this.direction = direction;
  this.angle = getDirectionAngle[direction];
  return this;
};

//** turn the robot 90 degrees, recives 'L' for left and 'R' for right
Robot.prototype.turn = function(where){
  if(where == 'L') {
    this.angle += 90;
    if(this.angle >= 360) this.angle = this.angle - 360;
    this.direction = (getAngleDirection[this.angle]);
  }else if(where == 'R') {
    this.angle -= 90;
    if(this.angle < 0) this.angle = this.angle + 360;
    this.direction = (getAngleDirection[this.angle]);
  }
};

//** moves the robot depending on the direction
Robot.prototype.move = function(){
  if(this.direction == 'N') this.y += 1;
  if(this.direction == 'S') this.y -= 1;
  if(this.direction == 'W') this.x -= 1;
  if(this.direction == 'E') this.x += 1;
};

module.exports = Robot;
